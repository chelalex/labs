package ru.mephi.hw5;

import java.time.*;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjuster;
import java.time.temporal.TemporalAdjusters;

import static java.time.temporal.ChronoUnit.*;

public class History {

    public static void main(String... args){
        aboutAbrahamLincoln();
        aboutBenedictCumberbatch();
        aboutTrainFromBoston();
        aboutFlightFromBoston();
        aboutSchoolSemester();
        flightTable();
    }

    public static void aboutAbrahamLincoln() {
        System.out.println("Lincoln, Abraham:");
        LocalDate lincolnBirthday = LocalDate.of(1809, 2, 12);
        LocalDate lincolnDeathday = LocalDate.of(1855, 4, 15);
        long yearsToDeath = lincolnBirthday.until(lincolnDeathday, YEARS);
        System.out.println("How old when he died? " + yearsToDeath);
        long daysToDeath = lincolnBirthday.until(lincolnDeathday, DAYS);
        System.out.println("How many days did he live? " + daysToDeath);
        System.out.println();
    }

    public static void aboutBenedictCumberbatch() {
        System.out.println("Cumberbatch, Benedict:");
        LocalDate cumberbatchBirthday = LocalDate.of(1976, 7, 19);
        System.out.println("Born in a leap year? " + cumberbatchBirthday.isLeapYear());
        System.out.println("How many days in the year he was born? " + cumberbatchBirthday.lengthOfYear());
        long decades = cumberbatchBirthday.until(LocalDate.now(), DECADES);
        System.out.println("How many decades old is he? " + decades);
        LocalDate twentyFirstBirthday = cumberbatchBirthday.plusYears(21);
        System.out.println("What was the day of the week on his 21st birthday? " + twentyFirstBirthday.getDayOfWeek());
        System.out.println();
    }

    public static void aboutTrainFromBoston() {
        System.out.println("Train from Boston:");
        ZoneId newYork = ZoneId.of("UTC-5");
        LocalTime dep = LocalTime.of(13,45);
        LocalTime arr = LocalTime.of(19, 25);
        ZonedDateTime departure = ZonedDateTime.of(LocalDate.now(), dep, newYork);
        ZonedDateTime arrival = ZonedDateTime.of(LocalDate.now(), arr, newYork);
        long minToArrive = departure.until(arrival, MINUTES);
        System.out.println("How many minutes long is the train ride? " + minToArrive);
        ZonedDateTime actualArrival = arrival.plusHours(1).plusMinutes(19);
        System.out.println("If the train was delayed 1 hour 19 minutes, what is the actual arrival time? " + actualArrival.toLocalTime());
        System.out.println();
    }

    public static void aboutFlightFromBoston() {
        System.out.println("Flight from Boston:");
        LocalDate date = LocalDate.of(2021, 3, 24);
        LocalTime time = LocalTime.of(21, 15);
        LocalDateTime departure = LocalDateTime.of(date, time);
        LocalDateTime arrivalTime = departure.plusHours(4).plusMinutes(15);
        ZonedDateTime arrival = ZonedDateTime.of(arrivalTime, ZoneId.of("UTC-5"));
        System.out.println("When does it arrive in Miami? " + arrival.toLocalTime());
        ZonedDateTime delayedArrival = arrival.plusHours(4).plusMinutes(27);
        System.out.println("When does it arrive if the flight is delays 4 hours 27 minutes? " + delayedArrival.toLocalTime());
        System.out.println();
    }

    public static void aboutSchoolSemester() {
        System.out.println("School semester:");
        TemporalAdjuster adjuster = TemporalAdjusters.dayOfWeekInMonth(2,DayOfWeek.TUESDAY);
        LocalDate septemberStart = LocalDate.of(2021, 9, 1);
        LocalDate secondTuesdayInSeptember = septemberStart.with(adjuster);
        LocalDate summerVacation = LocalDate.of(2022, 6, 25);
        System.out.println("What is the date? " + secondTuesdayInSeptember);
        final int start = secondTuesdayInSeptember.getDayOfWeek().getValue();
        final int end = summerVacation.getDayOfWeek().getValue();
        long semesterLength = DAYS.between(secondTuesdayInSeptember, summerVacation) - 28;
        long duration = semesterLength - 2*(semesterLength/7);
        if (semesterLength % 7 != 0) {
            if (end == 7) {
                duration -= 1;
            } else if (end == 1) {
                duration -= 2;
            }
        }
        System.out.println("How many days of school are there? " + duration);
        System.out.println();
    }

    public static void flightTable() {
        final ZoneId NEWYORK = ZoneId.of("America/New_York");
        final ZoneId LOSANGELES = ZoneId.of("America/Los_Angeles");
        final ZoneId CALCUTTA = ZoneId.of("Asia/Calcutta");

        System.out.println("Flight 123:");
        LocalTime depT = LocalTime.of(22, 30);
        LocalDate depD = LocalDate.of(2014, 6, 13);
        ZonedDateTime departure = ZonedDateTime.of(depD, depT, LOSANGELES);
        System.out.println("What is the local time in Boston when the flight takes off? " + departure.withZoneSameInstant(NEWYORK).toLocalTime());
        ZonedDateTime arrival = departure.withZoneSameInstant(NEWYORK).plusHours(5).plusMinutes(30);
        System.out.println("What is the local time at Boston Logan airport when the flight arrives? " + arrival.toLocalTime());
        arrival = arrival.withZoneSameInstant(LOSANGELES);
        System.out.println("What is the local time in San Francisco when the flight arrives? " + arrival.toLocalTime());
        System.out.println("============================================================================");
        System.out.println("Flight 456:");
        depT = LocalTime.of(22, 30);
        depD = LocalDate.of(2014, 6, 28);
        departure = ZonedDateTime.of(depD, depT, LOSANGELES);
        arrival = departure.plusHours(22).withZoneSameInstant(CALCUTTA);
        LocalTime meeting = LocalTime.of(9, 0);
        System.out.println("Will the traveler make a meeting in Bangalore Monday at 9 AM local time? " + arrival.toLocalTime().isBefore(meeting));
        arrival = arrival.withZoneSameInstant(LOSANGELES);
        LocalTime start = LocalTime.of(9, 0);
        LocalTime end = LocalTime.of(22, 0);
        System.out.println("Can the traveler call her husband at a reasonable time when she arrives? " + (arrival.toLocalTime().isAfter(start) && arrival.toLocalTime().isBefore(end)));
        System.out.println("============================================================================");
        System.out.println("Flight 123 2.0:");
        depT = LocalTime.of(22, 30);
        depD = LocalDate.of(2014, 10, 1);
        departure = ZonedDateTime.of(depD, depT, LOSANGELES);
        arrival = departure.plusHours(5).plusMinutes(30);
        System.out.println("What day and time does the flight arrive in Boston? " + arrival.toLocalDate() + " " + arrival.withZoneSameInstant(NEWYORK).toLocalTime());
    }
}
