package ru.mephi.lab5;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Exceptions {

    public static void main(String... args) throws IOException {
        checkIndex();
        findFile();
        isNull();
        isIOException();
        classCating();
        arithmeticProblems();
        catchMemoryError();
        stackOver();
    }

    public static void checkIndex() throws ArrayIndexOutOfBoundsException{
        int[] array = {1, 2, 3, 4, 5};
        try {
            int a = array[5];
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("out of the bounds, sorry");
        }
    }

    public static void findFile() throws IOException {
        try {
            BufferedReader reader = new BufferedReader(new FileReader("/Users/aleksandracelnokova/Desktop/hello"));
            String first = reader.readLine();
            System.out.println(first);
        } catch (FileNotFoundException e) {
            System.out.println("no such file, sorry");
        }
    }

    public static void isNull() throws NullPointerException {
        Integer a = null;
        try {
            a += 1;
        } catch (NullPointerException e) {
            System.out.println("how should we add 1 to null????");
        }
    }

    public static void isIOException() throws IOException {
        try {
            FileWriter writer = new FileWriter("/Users/aleksandracelnokova/Desktop/out.txt");
            writer.close();
            writer.write("first line");
        } catch (IOException e) {
            System.out.println("you closed file before writing");
        }
    }

    public static void classCating() throws ClassCastException {
        Object a = Integer.valueOf(11);
        try {
            String b = (String) a;
        } catch (ClassCastException e) {
            System.out.println("integer is not string, man");
        }
    }

    public static void arithmeticProblems() throws ArithmeticException {
        int a = 10;
        int b = 0;
        try {
            System.out.println(a/b);
        } catch (ArithmeticException e) {
            System.out.println("divided by zero");
        }
    }

    public static void catchMemoryError() {
        try {
            Integer[] array = new Integer[1000*1000*1000];
        } catch (OutOfMemoryError e) {
            System.out.println(e.getMessage());
        }
    }

    public static void infinity(){
        infinity();
    }

    public static void stackOver() {
        try {
            infinity();
        } catch (StackOverflowError e) {
            System.out.println(e.getMessage());
        }
    }
}
