package ru.mephi.lab4;

import ru.mephi.lab3.Employee;

import java.util.Comparator;
import java.util.List;

import static ru.mephi.lab3.gender.*;
import static ru.mephi.lab3.role.*;

public class Salaries {
    public static void main(String... args) {
        List<Employee> dreamTeam = Employee.createShortList();

        //map examples
        dreamTeam.stream()
                .map(e -> e.getGivenName() + ' ' + e.getSurName())
                .forEach(System.out::println);

        dreamTeam.stream()
                .map(e -> e.getSalary())
                .forEach(System.out::println);

        //peek examples

        dreamTeam.stream()
                .filter(e -> e.getGender().equals(FEMALE))
                .peek(e -> System.out.println(e.getSurName() + " is a female"))
                .forEach(System.out::println);

        dreamTeam.stream()
                .filter(e -> e.getSalary() > 50000)
                .peek(e -> System.out.println(e.getSurName() + " gets more than 50000"))
                .forEach(System.out::println);

        //findFirst examples
        System.out.println(
                dreamTeam.stream()
                        .filter(e -> e.getRole() == STAFF)
                        .findFirst()
        );

        System.out.println(
                dreamTeam.stream()
                        .filter(e -> e.getCity().equals("Liverpool"))
                        .findFirst()
        );

        //lazy examples

        dreamTeam.stream()
                .filter(e -> e.getRole().equals(MANAGER))
                .peek(e -> System.out.println(e.getSalary() + " is for managers"))
                .filter(e -> e.getAge() > 35)
                .peek(e -> System.out.println(e.getSalary() + " is for elder workers"))
                .forEach(System.out::println);

        dreamTeam.stream()
                .filter(e -> e.getRole() == MANAGER)
                .peek(e -> System.out.println(e.getCity() + " is city of managers"))
                .filter(e -> e.getAge() > 35)
                .peek(e -> System.out.println(e.getCity() + " is city of elder workers"))
                .forEach(System.out::println);

        //max examples
        System.out.println(
                dreamTeam.stream()
                        .mapToInt(e -> e.getSalary())
                        .max().getAsInt()
        );

        System.out.println(
                dreamTeam.stream()
                        .mapToInt(e -> e.getAge())
                        .max().getAsInt()
        );

        //min examples
        System.out.println(
                dreamTeam.stream()
                        .mapToInt(e -> e.getSalary())
                        .min().getAsInt()
        );

        System.out.println(
                dreamTeam.stream()
                        .mapToInt(e -> e.getAge())
                        .min().getAsInt()
        );

        //average examples
        System.out.println(
                dreamTeam.stream()
                        .mapToInt(e -> e.getSalary())
                        .average().getAsDouble()
        );

        System.out.println(
                dreamTeam.stream()
                        .mapToInt(e -> e.getAge())
                        .average().getAsDouble()
        );

        //sum examples
        System.out.println(
                dreamTeam.stream()
                        .mapToInt(e -> e.getSalary())
                        .sum()
        );

        System.out.println(
                dreamTeam.stream()
                        .mapToInt(e -> e.getAge())
                        .sum()
        );


    }
}
