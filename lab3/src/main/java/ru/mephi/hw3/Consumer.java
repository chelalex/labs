package ru.mephi.hw3;

import ru.mephi.lab3.Employee;

public interface Consumer{
    void accept(Employee employee);
}