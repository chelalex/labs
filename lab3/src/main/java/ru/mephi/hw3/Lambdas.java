package ru.mephi.hw3;

import ru.mephi.lab3.Employee;

import java.util.List;

import static ru.mephi.lab3.gender.*;
import static ru.mephi.lab3.role.*;

public class Lambdas {

    public static void main(String... args){
        List<Employee> myList = Employee.createShortList();
        BiPredicate isStaffAndFromCity = (t, city) -> t.getRole().equals(STAFF) && t.getCity().equals(city);
        for (Employee e:myList) {
            if (isStaffAndFromCity.test(e, "Moscow")){
                Employee.Accountant.paySalary(e);
            }
        }
        System.out.println(System.lineSeparator());

        Consumer promotion = t ->
        {
            if (t.getRole().equals(MANAGER)){
                t.setRole(EXECUTIVE);
            }
            else if (t.getRole().equals(STAFF)){
                t.setRole(MANAGER);
            }
            System.out.println(t.getGivenName() + " " + t.getSurName() + " is promoted up to " + t.getRole());
        };
        for (Employee e:myList) {
            promotion.accept(e);
        }
        System.out.println(System.lineSeparator());

        Consumer birthday = t ->
        {
            t.setAge(t.getAge() + 1);
            System.out.println(t.getGivenName() + " " + t.getSurName() + " celebrates their birthday! Their age is " + t.getAge());
        };
        birthday.accept(myList.get(1));
        System.out.println(System.lineSeparator());

        Function introduction = t -> "My name is " + t.getGivenName() + " " + t.getSurName();
        for (Employee e:myList) {
            System.out.println(introduction.apply(e));
        }
        System.out.println(System.lineSeparator());

        Function giveNumber = t -> "Call me, please: " + t.getPhone();
        for (Employee e:myList) {
            System.out.println(giveNumber.apply(e));
        }
        System.out.println(System.lineSeparator());

        Supplier begginer =
                () -> new Employee.Builder()
                        .withGivenName("Ivan")
                        .withSurName("Ivanov")
                        .withAge(18)
                        .withRole(STAFF)
                        .withGender(MALE)
                        .withAddress("1st Prospect, 69")
                        .withCity("Saint Petersburg")
                        .withState("Leningradskaya")
                        .withDept("IT")
                        .withPhone(12345)
                        .withCode((short) 111)
                        .withEMail("hello@bla.com")
                        .build();

        Supplier experienced =
                () -> new Employee.Builder()
                        .withGivenName("Albert Petrovich")
                        .withSurName("Samoylov")
                        .withAge(99)
                        .withRole(EXECUTIVE)
                        .withGender(MALE)
                        .withAddress("19st Prospect, 1169")
                        .withCity("Saint Petersburg")
                        .withState("Leningradskaya")
                        .withDept("IT")
                        .withPhone(12345)
                        .withCode((short) 111)
                        .withEMail("getready@bla.com")
                        .build();
        myList.add(begginer.get());
        myList.add(experienced.get());
        int size = myList.size();
        System.out.println(myList.get(size - 2).toString());
        System.out.println(myList.get(size - 1).toString());
    }
}
