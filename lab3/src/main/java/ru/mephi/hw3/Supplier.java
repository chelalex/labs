package ru.mephi.hw3;


import ru.mephi.lab3.Employee;

public interface Supplier{
    Employee get();
}