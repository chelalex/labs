package ru.mephi.lab3;

import java.util.ArrayList;
import java.util.List;

import static ru.mephi.lab3.gender.*;

import static ru.mephi.lab3.role.*;

public class Employee {

    private String givenName;
    private String surName;
    private int age;
    private gender myGender;
    private role myRole;
    private String dept;
    private String eMail;
    private long phone;
    private String address;
    private String city;
    private String state; //область
    private short code; //код области
    private int salary;

    public static class Builder {
        private Employee newEmployee;

        public Builder() {
            newEmployee = new Employee();
        }

        public Builder withGivenName(String givenName) {
            newEmployee.givenName = givenName;
            return this;
        }

        public Builder withSurName(String surName) {
            newEmployee.surName = surName;
            return this;
        }

        public Builder withAge(int age) {
            if (age > 0) {
                if (age >= 100) {
                    age = 99;
                }
                newEmployee.age = age;
                return this;
            } else {
                throw new IllegalArgumentException("age is less than zero");
            }
        }

        public Builder withGender(gender newGender) {
            newEmployee.myGender = newGender;
            return this;
        }

        public Builder withRole(role newRole) {
            newEmployee.myRole = newRole;
            return this;
        }

        public Builder withDept(String dept) {
            newEmployee.dept = dept;
            return this;
        }

        public Builder withEMail(String eMail) {
            newEmployee.eMail = eMail;
            return this;
        }

        public Builder withPhone(long phone) {
            newEmployee.phone = phone;
            return this;
        }

        public Builder withAddress(String address) {
            newEmployee.address = address;
            return this;
        }

        public Builder withCity(String city) {
            newEmployee.city = city;
            return this;
        }

        public Builder withState(String state) {
            newEmployee.state = state;
            return this;
        }

        public Builder withCode(short code) {
            if (code >= 0) {
                newEmployee.code = code;
                return this;
            } else {
                throw new IllegalArgumentException("state code is less than zero");
            }
        }

        public Builder withSalary(int salary) {
            if (salary > 0) {
                newEmployee.salary = salary;
                return this;
            }
            else {
                throw new IllegalArgumentException("Salary is less then zero");
            }
        }
        public Employee build() {
            return newEmployee;
        }


    }

    @Override
    public String toString() {
        return "Employee{"
                + "givenName = '" + givenName + '\''
                + ", surName = '" + surName + '\''
                + ", age = '" + age + '\''
                + ", gender = '" + myGender + '\''
                + ", role = '" + myRole + '\''
                + ", dept = '" + dept + '\''
                + ", eMail = '" + eMail + '\''
                + ", phone = '" + phone + '\''
                + ", address = '" + address + '\''
                + ", city = '" + city + '\''
                + ", state = '" + state + '\''
                + ", code = '" + code + '\''
                + ", salary = '" + salary + '\''
                + "}";
    }

    public String getGivenName() {
        return givenName;
    }

    public String getSurName() {
        return surName;
    }

    public gender getGender() {
        return myGender;
    }

    public int getAge() {
        return age;
    }

    public role getRole() {
        return myRole;
    }

    public long getPhone() {
        return phone;
    }

    public String getDept() {
        return dept;
    }

    public short getCode() {
        return code;
    }

    public String getAddress() {
        return address;
    }

    public String getCity() {
        return city;
    }

    public String getEMail() {
        return eMail;
    }

    public String getState() {
        return state;
    }

    public int getSalary() { return salary; }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setAge(int age) {
        if (age > 0) {
            if (age >= 100) {
                age = 99;
            }
            this.age = age;
        } else {
            throw new IllegalArgumentException("age is less than zero");
        }
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setCode(short code) {
        this.code = code;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    public void setEMail(String eMail) {
        this.eMail = eMail;
    }

    public void setGender(gender newGender) {
        this.myGender = newGender;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public void setPhone(long phone) {
        this.phone = phone;
    }

    public void setRole(role mewRole) {
        this.myRole = mewRole;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public void setSalary(int salary) { this.salary = salary; }

    public static List<Employee> createShortList() {
        List<Employee> myList = new ArrayList<Employee>();
        String[] givenNames = {"Ivan", "Andrew", "Michael", "John", "Egor", "Anastasia", "Oleg"};
        String[] surNames = {"Bruslavskiy", "Laushkin", "Jackson", "Lennon", "Dmitriev", "Kolpakova", "Tinkoff"};
        int[] ages = {19, 18, 62, 1000, 20, 20, 50};
        gender[] genders = {MALE, MALE, MALE, MALE, MALE, FEMALE, MALE};
        role[] roles = {STAFF, STAFF, STAFF, MANAGER, STAFF, MANAGER, EXECUTIVE};
        String[] depts = {"Plasma", "Medicine", "Music", "Music", "IT", "Information Security", "Finance"};
        String[] eMails = {"123@bla.com", "234@bla.com", "345@bla.com", "456@bla.com", "567@bla.com", "678@bla.com", "789@bla.com"};
        long[] phones = {123456789, 876542248, 234567811, 865469763, 753237097, 827664825, 999572532};
        String[] addresses = {"1st Street, 2", "2nd Street, 3", "3rd Street, 4", "4th Street, 5", "5th Street, 6", "6th Street, 7", "7th Street, 8"};
        String[] cities = {"Moscow", "Ekaterinburg", "Berkeley", "Liverpool", "Moscow", "Moscow", "New York"};
        String[] states = {"Moscow", "Sverdlovskaya", "California", "Merseyside", "Moscow", "Moscow", "New York"};
        short[] codes = {495, 343, 1925, 4415, 495, 495, 1631};
        int[] salaries = {10000, 124352, 43521, 54367, 23456, 87692, 21900};
        for (int i = 0; i < 7; i++) {
            Employee employee = new Builder()
                    .withGivenName(givenNames[i])
                    .withSurName(surNames[i])
                    .withAge(ages[i])
                    .withGender(genders[i])
                    .withRole(roles[i])
                    .withDept(depts[i])
                    .withEMail(eMails[i])
                    .withPhone(phones[i])
                    .withAddress(addresses[i])
                    .withCity(cities[i])
                    .withState(states[i])
                    .withCode(codes[i])
                    .withSalary(salaries[i])
                    .build();
            myList.add(employee);
        }
        return myList;
    }

    public static class Accountant {

        public static void paySalary(Employee employee) {
            System.out.println("The salary for " + employee.getGivenName() + " " + employee.getSurName() + " is paid: " + employee.salary);
        }

        public static void payPremium(Employee employee) {
            double bonus = 0;
            String strBonus = "";
            if (employee.getRole().equals(STAFF)) {
                bonus = 0.1;
                strBonus = "10%";
            }
            else if (employee.getRole().equals(MANAGER)) {
                bonus = 0.2;
                strBonus = "20%";
            }
            else if (employee.getRole().equals(EXECUTIVE)) {
                bonus = 0.3;
                strBonus = "30%";
            }
            System.out.println("The premium of " + strBonus + " for " + employee.getGivenName() + " " + employee.getSurName() + " is paid: " + employee.salary * bonus);
        }
    }

    public static void main(String... args) {
        List<Employee> myList = createShortList();
        myList.stream()
                .filter(x -> x.getGender().equals(FEMALE))
                .forEach(x -> Accountant.payPremium(x));
        System.out.println("");
        myList.stream()
                .filter(x -> x.getDept().equals("Music"))
                .forEach(x -> Accountant.paySalary(x));
        System.out.println("");
        myList.stream()
                .filter(x -> x.getAge() > 30 && x.getDept().equals("IT"))
                .forEach(x -> Accountant.payPremium(x));
        System.out.println("");
        myList.stream()
                .filter(x -> x.getRole().equals(MANAGER))
                .forEach(x -> Accountant.paySalary(x));
        System.out.println("");
        myList.stream()
                .filter(x -> x.getRole().equals(STAFF))
                .forEach(x -> Accountant.payPremium(x));
    }
}
