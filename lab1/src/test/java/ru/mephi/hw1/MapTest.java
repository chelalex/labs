package ru.mephi.hw1;

import org.junit.jupiter.api.Test;
import ru.mephi.lab1.ListStud;

import static org.junit.jupiter.api.Assertions.*;

class MapTest {

    private final Map myMap;

    MapTest() {
        MapNode one = new MapNode(1,2);
        MapNode two = new MapNode("3", "4");
        myMap = new Map(one, two);
    }

    @Test
    void keyContains() {
        assertTrue(myMap.keyContains(1));
        assertTrue(myMap.keyContains("3"));
        assertFalse(myMap.keyContains(2));
        assertFalse(myMap.keyContains("4"));
    }

    @Test
    void get() {
        assertNull(myMap.get(3));
        assertNotNull(myMap.get("3"));
        assertNotNull(myMap.get(1));
        assertNull(myMap.get("1"));
    }

    @Test
    void testGet() {
        assertEquals("test", myMap.get(2, "test"));
        assertEquals("nothing", myMap.get(4, "nothing"));
        assertEquals(2, myMap.get(1, "nothing"));
        assertEquals("4", myMap.get("3", "nothing"));
    }

    @Test
    void remove() {
        assertNull(myMap.remove(35));
        assertNull(myMap.remove("asdfghj"));
        assertNotNull(myMap.remove(1));
        assertNotNull(myMap.remove("3"));
    }

    @Test
    void getKeys() {
        ListStud keys = new ListStud(1, "3");
        assertEquals(keys.get(0), myMap.getKeys().get(0));
        assertEquals(keys.get(1), myMap.getKeys().get(1));
    }

    @Test
    void getValues() {
        ListStud values = new ListStud(2, "3");
        assertEquals(values.get(0), myMap.getValues().get(0));
        assertNotEquals(values.get(1), myMap.getValues().get(1));
    }

    @Test
    void getEntries() {
        MapNode one = new MapNode(1,2);
        MapNode two = new MapNode("3", "4");
        assertNotNull(myMap.getEntries().get(0));
        assertNotNull(myMap.getEntries().get(1));
    }

    @Test
    void size() {
        assertEquals(2, myMap.size());
        myMap.remove(1);
        myMap.remove("3");
        assertEquals(0, myMap.size());
    }

    @Test
    void isEmpty() {
        assertFalse(myMap.isEmpty());
        myMap.remove(1);
        myMap.remove("3");
        assertTrue(myMap.isEmpty());
    }
}