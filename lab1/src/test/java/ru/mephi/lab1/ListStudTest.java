package ru.mephi.lab1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

class ListStudTest {

    private final ListStud myList = new ListStud(2,"3",4,5,6,7,8,9,"10");
    @Test
    void remove() {
        assertEquals(2,myList.remove(0));
        assertEquals("3", myList.remove(0));
        assertEquals("10", myList.remove(100));
        assertNotEquals(0, myList.remove(1));
    }

    @Test
    void get() {
        assertEquals(2, myList.get(0));
        assertNotEquals(1, myList.get(0));
        assertNotEquals("aaa", myList.get(50));
        assertEquals("10", myList.get(50));
    }

    @Test
    void indexOf() {
        assertEquals(-1, myList.indexOf("aaa"));
        assertEquals(8, myList.indexOf("10"));
        assertEquals(-1, myList.indexOf(10));
        assertNotEquals(5, myList.indexOf(6));
    }

    @Test
    void contains() {
        assertEquals(true, myList.contains("3"));
        assertEquals(true, myList.contains(2));
        assertEquals(false, myList.contains(1000));
        assertEquals(false, myList.contains(9.9));
        assertEquals(true, myList.contains(7));
    }

    @Test
    void set() {
        assertEquals(2, myList.set("2", 0));
        assertEquals("2", myList.set("222", -10));
        assertEquals("10", myList.set(10, 1000));
        assertEquals(10, myList.set("1000", 8));
    }

    @Test
    void size() {
        assertEquals(9, myList.size());
        myList.remove(10);
        myList.remove(-2);
        assertEquals(7, myList.size());
    }

    @Test
    void isEmpty() {
        assertNotEquals(true, myList.isEmpty());
        myList.remove(8);
        myList.remove(8);
        myList.remove(8);
        myList.remove(8);
        myList.remove(8);
        myList.remove(8);
        myList.remove(8);
        myList.remove(8);
        myList.remove(8);
        assertEquals(true, myList.isEmpty());
    }
}