package ru.mephi.lab1;

public class ListStud {
    private Object[] values;
    private int size;
    private final int CAPACITY = 16;

    public static void main(String... args) {
        ListStud myObj1 = new ListStud();
        Object a = new String("a");
        ListStud myObj2 = new ListStud();
        myObj2.add("a");
        myObj2.add(60, 70);
        ListStud myObj3 = new ListStud("a", 1, "bbb", "asdfghj");
        System.out.println(myObj3.size());
        System.out.println(myObj3.remove(0));
        System.out.println(myObj3.get(0));
        System.out.println(myObj3.size());
        System.out.println(myObj1.size());
    }

    private void ascendCapacity() {
        if (this.size % CAPACITY == 0) {
            Object[] array = new Object[this.size + CAPACITY];
            if (this.size > 0) {
                System.arraycopy(this.values, 0, array, 0, this.size);
            }
            this.values = new Object[this.size + CAPACITY];
            System.arraycopy(array, 0, this.values, 0, this.size);
        }
    }

    private void decendCapacity() {
        if (this.size % CAPACITY == 0) {
            Object[] array = new Object[this.size];
            if (this.size > 0) {
                System.arraycopy(this.values, 0, array, 0, this.size);
            }
            this.values = new Object[this.size];
            System.arraycopy(array, 0, this.values, 0, this.size);
        }
    }


    public ListStud() {
        this.size = 0;
        ascendCapacity();
    }

    public ListStud(Object... values) {
        if (values != null) {
            int length = values.length;
            //this.values = values;
            //this.size = length;
            int index = 0;
            while (length > 0) {
                ascendCapacity();
                int bound = length > CAPACITY ? CAPACITY : length;
                System.arraycopy(values, index, this.values, index, bound);
                index += CAPACITY;
                length -= CAPACITY;
                this.size += bound;
            }
            //this.size += values.length;
        } else {
            this.size = 0;
            ascendCapacity();
        }
    }

    /**
     * add new value at the end of list
     *
     * @param value
     */
    public void add(Object value) {
        if (value != null) {
            int size = this.size;
            this.ascendCapacity();
            this.values[this.size] = value;
            this.size++;
        }
    }

    public void add(int index, Object value) {
        if (value != null) {
            //int length = values.length;
            int size = this.size;
            this.add(value);
            if (index > size - 1) {
                index = size - 1;
            }
            if (index < 0) {
                index = 0;
            }
            this.size++;
            Object temp = this.values[this.size - 1];
            this.values[this.size - 1] = this.values[index];
            this.values[index] = temp;
        }
    }

    /**
     * remove an element by specified index in list
     *
     * @param index
     * @return Object
     */
    public Object remove(int index) {
        Object removed = null;
        int size = this.size;
        if (size > 0) {
            Object[] array = new Object[size - 1];
            if (index >= size - 1) {
                index = size - 1;
                System.arraycopy(this.values, 0, array, 0, size - 1);
            }
            if (index <= 0) {
                index = 0;
                System.arraycopy(this.values, 1, array, 0, size - 1);
            }
            if (0 < index && index < size - 1) {
                System.arraycopy(this.values, 0, array, 0, index);
                System.arraycopy(this.values, index + 1, array, index, size - index - 1);
            }
            removed = this.values[index];
            this.size--;
            System.arraycopy(array, 0, this.values, 0, this.size);
            decendCapacity();
        }
        return removed;
    }

    public Object get(int index) {
        Object searched = null;
        int size = this.size;
        if (size > 0) {
            if (index > size - 1) {
                index = size - 1;
            }
            if (index < 0) {
                index = 0;
            }
            searched = this.values[index];
        }
        return searched;
    }

    public int indexOf(Object value) {
        int index = -1;
        int size = this.size;
        for (int i = 0; i < size; i++) {
            if (this.values[i] == value) {
                index = i;
                break;
            }
        }
        return index;
    }

    public boolean contains(Object value) {
        boolean flag = false;
        int index = this.indexOf(value);
        return index >= 0;
    }

    public Object set(Object value, int index) {
        Object changed = null;
        int size = this.size;
        if (size > 0) {
            if (index > size - 1) {
                index = size - 1;
            }
            if (index < 0) {
                index = 0;
            }
            changed = this.values[index];
            this.values[index] = value;
        }
        return changed;
    }

    public int size() {
        return this.size;
    }

    public boolean isEmpty() {
        return this.size <= 0;
    }
}