package ru.mephi.hw1;

import ru.mephi.lab1.ListStud;

public class Map {
    private ListStud items;
    private int size;

    public static void main(String... args){
        MapNode item = new MapNode(2, "aaa");
        Map myMap = new Map(item);
        boolean flag = myMap.keyContains(2);
        Object check = myMap.get(2);
        System.out.println(flag + " " + check);
        myMap.put(2, "bbb");
        ListStud list = myMap.getEntries();
        for (int i = 0; i < list.size(); i++) {
            MapNode innerItem = (MapNode) list.get(i);
            System.out.println(innerItem.getKey() + " " + innerItem.getValue());
        }
    }
    public Map(){
        this.items = null;
        this.size = 0;
    }

    public Map(MapNode... items){
        this.items = new ListStud(items);
        this.size = items.length;
    }

    public boolean keyContains(Object key){
        boolean flag = false;
        int size = this.size;
        System.out.println();
        if (size > 0 && key != null) {
            for (int i = 0; i < size; i++) {
                MapNode item = (MapNode) this.items.get(i);
                //MapNode other = new MapNode(key, item.value);
                if (item.getValue() != null && item.getKey().equals(key)) {
                    flag = true;
                    break;
                }
            }
        }
        return flag;
    }

    public void put(Object key, Object value){
        if (key != null && value != null) {
            if (!this.keyContains(key)) {
                MapNode newItem = new MapNode(key, value);
                this.items.add(newItem);
                this.size++;
            }
            else {
                for (int i = 0; i < this.size; i++) {
                    MapNode item = (MapNode) this.items.get(i);
                    //MapNode newItem = new MapNode(key, item.getValue());
                    if (item.getKey().equals(key)){
                        item.setValue(value);
                        //this.items.add(i, newItem);
                        break;
                    }
                }
            }
        }
    }
    public Object get (Object key){
        Object got = null;
        int size = this.size;
        if (size > 0 && key != null){
            for (int i = 0; i < size; i++){
                MapNode item = (MapNode) this.items.get(i);
                if (item.getKey().equals(key)){
                    got = item.getValue();
                }
            }
        }
        return got;
    }

    public Object get(Object key, Object byDefault) {
        Object got = byDefault;
        int size = this.size;
        if (size > 0 && key != null){
            for (int i = 0; i < size; i++){
                MapNode item = (MapNode) this.items.get(i);
                if (item.getKey().equals(key)){
                    got = item.getValue();
                }
            }
        }
        return got;
    }

    public Object remove(Object key) {
        Object removed = null;
        int size = this.size;
        if (size > 0 && key != null){
            for (int i = 0; i < size; i++){
                MapNode item = (MapNode) this.items.get(i);
                if (item.getKey().equals(key)){
                    removed = item.getValue();
                    this.items.remove(i);
                    this.size--;
                    break;
                }
            }
        }
        return removed;
    }

    public ListStud getKeys() {
        ListStud keys = new ListStud();
        if (size > 0){
            for(int i = 0; i < size; i++){
                MapNode item = (MapNode) this.items.get(i);
                keys.add(item.getKey());
            }
        }
        return keys;
    }

    public ListStud getValues() {
        ListStud values = new ListStud();
        if (size > 0){
            for(int i = 0; i < size; i++){
                MapNode item = (MapNode) this.items.get(i);
                values.add(item.getValue());
            }
        }
        return values;
    }

    public ListStud getEntries() {
        return this.items;
    }

    public int size() {
        return this.size;
    }

    public boolean isEmpty() {
        return this.size <= 0;
    }
}
