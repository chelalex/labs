package ru.mephi.hw1;

public class MapNode {
    private Object value;
    private Object key;

    public MapNode(){
        this.key = null;
        this.value = null;
    }

    public MapNode(Object key, Object value){
        this.key = key;
        this.value = value;
    }

    /*public boolean equals(MapNode item){
        return (this.key == item.key && this.value == item.value);
    }*/
    public Object getKey(){
        return this.key;
    }

    public Object getValue(){
        return this.value;
    }

    public void setKey(Object key){
        this.key = key;
    }

    public void setValue(Object value){
        this.value = value;
    }
}
