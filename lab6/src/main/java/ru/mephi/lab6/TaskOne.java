package ru.mephi.lab6;

import static java.lang.System.out;
import java.io.*;
import java.util.*;

public class TaskOne {
    static int count = 0;

    public static void main(String... args) throws Exception {
        Scanner commandLine = new Scanner(System.in);
        out.println("Enter line:");
        String line = commandLine.nextLine();
        BufferedReader reader = new BufferedReader(new FileReader(args[0]));
        try (reader) {
            reader
                    .lines()
                    .filter(x -> x.equals(line))
                    .forEach(x -> count++);
            out.println("The line repeated " + count + " time(-s) in the file.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
