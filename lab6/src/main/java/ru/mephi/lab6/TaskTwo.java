package ru.mephi.lab6;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class TaskTwo {
    public static void main(String... args) {
        String path = args[0];
        ShoppingCart cart1 = ShoppingCart.randomShoppingCart();
        cart1.serialize(path);
        ShoppingCart cart2 = ShoppingCart.deserialize(path);

        System.out.println("Before serialization:");
        cart1.output();

        System.out.println("==============================\nAfter serialization:");
        cart2.output();
    }

    static class ShoppingCart implements Serializable {
        static class Item implements Serializable {
            private final int id;
            private final String description;
            private final double cost;

            public Item(int newId, String newDescr, double newCost) {
                id = newId;
                description = newDescr;
                cost = newCost;
            }
            @Override
            public String toString() {
                return  id + "\t" + description + "\t" + cost;
            }

            public double getCost() {
                return cost;
            }

            public int getId() {
                return id;
            }

            public String getDescription() {
                return description;
            }
        }

        private final int cartID;
        public List<Item> items;
        private int itemCount;
        transient private double cartTotal;

        public ShoppingCart() {
            Random random = new Random();
            cartID = Math.abs(random.nextInt() % 100);
            items = new ArrayList<Item>();
            itemCount = 0;
            cartTotal = 0;
        }

        public int getCartID() {
            return cartID;
        }

        public ArrayList<Item> getItems() {
            return items;
        }

        public double getCartTotal() {
            return cartTotal;
        }

        public int getItemCount() {
            return itemCount;
        }

        public void setCartTotal(double cartTotal) {
            this.cartTotal = cartTotal;
        }

        /**
         *
         * @param path - path to file
         */
        public void serialize (String path) {
            try {
                FileOutputStream file = new FileOutputStream(path);
                ObjectOutputStream obj = new ObjectOutputStream(file);
                obj.writeObject(this);
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }

        public static ShoppingCart deserialize (String path) {
            ShoppingCart cart = null;
            try {
                FileInputStream file = new FileInputStream(path);
                ObjectInputStream obj = new ObjectInputStream(file);
                double cartTotal = 0;
                cart = (ShoppingCart) obj.readObject();
                for (int i = 0; i < cart.getItemCount(); i++) {
                    cartTotal += cart.getItems().get(i).getCost();
                }
                cart.setCartTotal(cartTotal);
            } catch (IOException | ClassNotFoundException e) {
                System.out.println(e.getMessage());
            }
            return cart;
        }

        public void put(Item item) {
            items.add(item);
            itemCount++;
            cartTotal += item.getCost();
        }

        public static ShoppingCart randomShoppingCart() {
            Random random = new Random();
            Item[] list = new Item[]{
                    new Item(1, "potatoes", 57.90),
                    new Item(2, "tomatoes", 89.99),
                    new Item(3, "cabbage", 56.79),
                    new Item(4, "carrot", 87.48),
                    new Item(5, "cucumbers", 62.65),
                    new Item(6, "apples", 219.78),
                    new Item(7, "pears", 129.80),
                    new Item(8, "plums", 187.20),
                    new Item(9, "garlics", 36.50),
                    new Item(10, "pepper", 124.80),
            };
            ShoppingCart newCart = new ShoppingCart();
            for (int i = 0; i < 5; i++) {
                newCart.put(list[Math.abs(random.nextInt() % list.length)]);
            }
            return newCart;
        }

        public void output() {
            System.out.println("cartID: " + cartID + "\nTotal: " + cartTotal + "\nCount of items: " + itemCount);
            System.out.println("id\tdescription\tcost\t");
            for (int i = 0; i < itemCount; i++) {
                System.out.println(items.get(i).toString());
            }
        }
    }
}
