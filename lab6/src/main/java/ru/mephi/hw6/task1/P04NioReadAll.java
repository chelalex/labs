package ru.mephi.hw6.task1;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Stream;

public class P04NioReadAll {
  public static void main() {
        
    Path file =  Paths.get("/Users/aleksandracelnokova/Desktop/Java/Files/hamlet.txt");
    List<String> fileArr;
    try{
        // Read fileinto array here
        fileArr = Files.readAllLines(file, StandardCharsets.UTF_8);
        System.out.println("\n=== Lord Count ===");
        long wordCount = 0; // Replace with your pipeline
        wordCount = fileArr.stream()
                .filter(x -> (x.contains("Lord") || (x.contains("lord"))))
                .count();

        System.out.println("Word count: " + wordCount);

        System.out.println("\n=== Prison Count ===");
        wordCount = 0; // Replace with your pipeline
        wordCount = fileArr.stream()
                .filter(x -> (x.contains("Prison")) || (x.contains("prison")))
                .count();

        System.out.println("Word count: " + wordCount);            

    }catch (IOException e){
        System.out.println("Error: " + e.getMessage());
    }

  } 
}
