package ru.mephi.hw6.task1;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

public class P02NioRead {
    
    public static void main(Path path) {

        try{ // Create Try with Resources here
            
            System.out.println("\n=== Entire File ===");
            // print lines here
            byte[] bytes = Files.readAllBytes(path);
            String lines = new String(bytes, StandardCharsets.UTF_8);
            System.out.println(lines);

        }catch (IOException e){
            System.out.println("Error: " + e.getMessage());
        }
    } 
}
