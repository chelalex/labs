package ru.mephi.hw6.task1;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;

public class P01BufferedReader {

    private static Path path;

    public static void main(Path path) {
        try{
            BufferedReader bReader = 
                new BufferedReader(new FileReader(path.toString()));
            
            System.out.println("=== Entire File ===");
            String line = null;
            do {
                line = bReader.readLine();
                System.out.println(line);
            } while (line != null);

        }catch (IOException e){
            System.out.println("Error: " + e.getMessage());
        }
    } 
}
