package ru.mephi.hw6.task1;

import java.nio.file.Path;
import java.nio.file.Paths;

public class Main {

    public static String strPath = "/Users/aleksandracelnokova/Desktop/Java/Files/hamlet.txt";
    public static Path path = Paths.get(strPath);
    public static void main(String[] args) {

        System.out.println("P01:");
        P01BufferedReader.main(path);
        System.out.println("\n==============================\nP02:");
        P02NioRead.main(path);
        System.out.println("\n==============================\nP03:");
        P03NioReadAll.main(path);
        System.out.println("\n==============================\nP04:");
        P04NioReadAll.main();
    } 
}
