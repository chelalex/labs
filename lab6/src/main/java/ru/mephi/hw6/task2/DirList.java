package ru.mephi.hw6.task2;

import java.io.File;
import java.util.Arrays;
import java.util.Objects;

public class DirList {
  public static void main(String[] args) {

    try { // Add Try with resources here

        System.out.println("\n=== Dir list ===");
        // Print directory list here
        Arrays.stream(Objects.requireNonNull(new File(".").listFiles()))
              .filter(File::isDirectory)
              .forEach(System.out::println);
    } catch (NullPointerException e){
        System.out.println("Error: " + e.getMessage());
    }

  } 
}
