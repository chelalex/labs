package ru.mephi.hw2;


public class MyList {
    private int[] values;
    private int size;
    private final int CAPACITY = 16;

    private void ascendCapacity() {
        if (this.size % CAPACITY == 0) {
            int[] array = new int[this.size + CAPACITY];
            if (this.size > 0) {
                System.arraycopy(this.values, 0, array, 0, this.size);
            }
            this.values = new int[this.size + CAPACITY];
            System.arraycopy(array, 0, this.values, 0, this.size);
        }
    }

    private void decendCapacity() {
        if (this.size % CAPACITY == 0) {
            int[] array = new int[this.size];
            if (this.size > 0) {
                System.arraycopy(this.values, 0, array, 0, this.size);
            }
            this.values = new int[this.size];
            System.arraycopy(array, 0, this.values, 0, this.size);
        }
    }


    public MyList() {
        this.size = 0;
        ascendCapacity();
    }

    public MyList(int... values) {
        if (values != null) {
            int length = values.length;
            //this.values = values;
            //this.size = length;
            int index = 0;
            while (length > 0) {
                ascendCapacity();
                int bound = length > CAPACITY ? CAPACITY : length;
                System.arraycopy(values, index, this.values, index, bound);
                index += CAPACITY;
                length -= CAPACITY;
                this.size += bound;
            }
            //this.size += values.length;
        } else {
            this.size = 0;
            ascendCapacity();
        }
    }

    /**
     * add new value at the end of list
     *
     * @param value
     */
    public void add(int value) {
        //if (value != null) {
            int size = this.size;
            this.ascendCapacity();
            this.values[this.size] = value;
            this.size++;
        //}
    }

    public void add(int index, int value) {
        //if (value != null) {
            //int length = values.length;
            int size = this.size;
            this.add(value);
            if (index > size - 1) {
                index = size - 1;
            }
            if (index < 0) {
                index = 0;
            }
            this.size++;
            int temp = this.values[this.size - 1];
            this.values[this.size - 1] = this.values[index];
            this.values[index] = temp;
        //}
    }

    /**
     * remove an element by specified index in list
     *
     * @param index
     * @return Object
     */
    public Object remove(int index) {
        Object removed = null;
        int size = this.size;
        if (size > 0) {
            int[] array = new int[size - 1];
            if (index >= size - 1) {
                index = size - 1;
                System.arraycopy(this.values, 0, array, 0, size - 1);
            }
            if (index <= 0) {
                index = 0;
                System.arraycopy(this.values, 1, array, 0, size - 1);
            }
            if (0 < index && index < size - 1) {
                System.arraycopy(this.values, 0, array, 0, index);
                System.arraycopy(this.values, index + 1, array, index, size - index - 1);
            }
            removed = this.values[index];
            this.size--;
            System.arraycopy(array, 0, this.values, 0, this.size);
            decendCapacity();
        }
        return removed;
    }

    public int get(int index) {
        int searched = Integer.MAX_VALUE;
        int size = this.size;
        if (size > 0) {
            if (index > size - 1) {
                index = size - 1;
            }
            if (index < 0) {
                index = 0;
            }
            searched = this.values[index];
        }
        return searched;
    }

    public int indexOf(int value) {
        int index = -1;
        int size = this.size;
        for (int i = 0; i < size; i++) {
            if (this.values[i] == value) {
                index = i;
                break;
            }
        }
        return index;
    }

    public boolean contains(int value) {
        boolean flag = false;
        int index = this.indexOf(value);
        return index >= 0;
    }

    public int set(int value, int index) {
        int changed = Integer.MAX_VALUE;
        int size = this.size;
        if (size > 0) {
            if (index > size - 1) {
                index = size - 1;
            }
            if (index < 0) {
                index = 0;
            }
            changed = this.values[index];
            this.values[index] = value;
        }
        return changed;
    }

    public int size() {
        return this.size;
    }

    public boolean isEmpty() {
        return this.size <= 0;
    }
}
