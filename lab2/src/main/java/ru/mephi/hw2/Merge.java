package ru.mephi.hw2;

import org.jetbrains.annotations.NotNull;
import ru.mephi.hw2.MyList;

public class Merge {

    public static void main(String... args){
        MyList myList1 = new MyList(9,9,3,2,1,5);
        MyList myList2 = new MyList(215, 20, 25);
        MyList newList = new MyList();
        try {
            newList = merge(myList1, myList2); //it should throw exception
        }
        catch (IllegalArgumentException e){
            System.out.println(e.getMessage());
        }
        sort(myList1);
        try {
            newList = merge(myList1, myList2);// should throw exception
        }
        catch (IllegalArgumentException e){
            System.out.println(e.getMessage());
        }
        sort(myList2);
        try {
            newList = merge(myList1, myList2); //it should merge correctly
        }
        catch (IllegalArgumentException e){
            System.out.println(e.getMessage());
        }
        for(int i = 0; i < newList.size(); i++){
            System.out.print(newList.get(i) + " ");
        }
    }
    public static void sort(@NotNull MyList myList){
        //if (myList.get(0).getClass().equals("java.lang.Integer")) {
            int size = myList.size();
            for (int step = size / 2; step > 0; step /= 2) {
                for (int i = 0; i < size; i++) {
                    for (int j = i + step; j < size; j += step) {
                        int numberI = myList.get(i);
                        int numberJ = myList.get(j);
                        if (numberI > numberJ) {
                            myList.set(numberJ, i);
                            myList.set(numberI, j);
                        }
                    }
                }
            }
       // }
        /*else{
            throw new IllegalArgumentException("It's not integer list");
        }*/
    }

    public static boolean isSorted(MyList myList){
        //if (myList.get(0).getClass().equals("java.lang.Integer")) {
            int size = myList.size();
            if (size == 1) return true;
            int aI = (int) myList.get(0);

            int i = 1;
            while (i < size && aI <= (aI = myList.get(i))) {
                i++;
            }
            return i >= size;
        //}
        /*else{
            throw new IllegalArgumentException("It's not integer list");
        }*/
    }

    public static MyList merge(@NotNull MyList list1, @NotNull MyList list2){
        //if (list1.get(0).getClass().equals("java.lang.Integer") && list2.get(0).getClass().equals("java.lang.Integer")) {
            MyList merged = new MyList();
            boolean flag = isSorted(list1) && isSorted(list2);
            if (flag) {
                int size1 = list1.size();
                int size2 = list2.size();
                int i = 0;
                int j = 0;
                while (i < size1 && j < size2) {
                    int item1 = list1.get(i);
                    int item2 = list2.get(j);
                    if (item1 < item2) {
                        merged.add(item1);
                        i++;
                    } else {
                        merged.add(item2);
                        j++;
                    }
                }
                if (i < size1) {
                    for (; i < size1; i++) {
                        int item1 = list1.get(i);
                        merged.add(item1);
                    }
                }

                if (j < size2) {
                    for (; j < size2; j++) {
                        int item2 = list2.get(j);
                        merged.add(item2);
                    }
                }

                return merged;
            }
            else{
                throw new IllegalArgumentException("There are unsorted lists");
            }
        //}
        /*else{
            throw new IllegalArgumentException("It's not integer list");
        }*/
    }
}
