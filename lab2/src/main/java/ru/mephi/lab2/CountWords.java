package ru.mephi.lab2;

import java.util.*;

public class CountWords {

    public static void main(String... args) {
        HashMap<String, Integer> dict = new HashMap<String, Integer>();
        Scanner in = new Scanner (System.in);
        if (in.hasNextLine()) {
            String line = in.nextLine();
        //if (line.length() > 0) {
            String newLine = line.replaceAll("\\s+"," ");//replace every extra spaces by single space
            String newNewLine = newLine.replaceAll("^\\s+|\\s+$", "");//replace extra spaces in the begin/end by nothing
            String[] words = newNewLine.split(" ");
            for (String word : words) {
                boolean flag = dict.containsKey(word);
                if (flag) {
                    Integer value = dict.get(word);
                    value++;
                    dict.put(word, value);
                }
                else {
                    dict.put(word, 1);
                }
            }
            if (words.length > 0) {
                Set<String> keys = dict.keySet();
                ArrayList<Integer> values = new ArrayList<>(dict.values());
                Iterator it_k = keys.iterator();
                Iterator it_v = values.iterator();
                while (it_k.hasNext() && it_v.hasNext()) {
                    System.out.println(it_k.next() + " : " + it_v.next());
                }
            }
            else {
                System.out.println("no words in line :(");
            }
        }
    }
}
